/**
 * Programmer: Meg Prescott
 * Date: 2017-09-21
 * CIS148 Week 4 Lab
 *
 * This class will simulate a pair of dice with possible face values of 1 through 6
 */
import java.util.Random;
public class Dice {
    //Member variables (fields)
    private int d1; //Stores value of first dye
    private int d2; //Stores value of second dye
    private Random random;

    //Constructor
    public Dice() {
        random = new Random();
        roll();
    }

    //Member methods
    public void roll() {
        d1 = random.nextInt(6) + 1;
        d2 = random.nextInt(6) + 1;
    }

    public int getD1() { return d1; }
    public int getD2() { return d2; }
    public int getTotal() { return d1+d2; }

}
